export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'DASHBOARD'  : 'Dashboard',
            'Appointments': 'Appointments',
            'Patients':'Patients',
            'Medications':'Medications',
            'Payments':'Payments',
             'Settings':'Settings',
            'HELP':'Help',
            'SUPPORT':'About',
            'USERS':'Users',
            'Infos':'Medical Infos',
            'Database':'Database'
        }
    }
};
