import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'app/main/authentication/login/authentication.service';
import {API_URL }from "app/_shared/config/api.url.config";

@Injectable()
export class JwtInterceptor //implements HttpInterceptor
 {

  constructor(private authService: AuthenticationService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    /*if(request.url!=API_URL.api_login){
      const token = this.authService.getToken();
      if (token) {
        request = request.clone({
          setHeaders: {
            Authorization: token
          }
        })
      } else {
        this.authService.logout();
      }
    }*/
    

    return next.handle(request);
  }
}
