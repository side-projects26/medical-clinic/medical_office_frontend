import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import {Router} from "@angular/router";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private router:Router) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return next.handle(request)
      .pipe(
        catchError(err => {
          // Here you can handle your errors
          // if([401,403].indexOf(err.status)!=-1){
            console.error(err)
          switch (err.status) {

            case 401:
              console.log('handle error 401 from MyErrorInterceptor');

              break;
            case 400:
              console.log('handle error 400 from MyErrorInterceptor');
              break;
              case 404:
                  this.router.navigateByUrl('/error-404')
                  break
              case 500:
                  this.router.navigateByUrl('/errors/error-500')
                  break
          }
          return throwError(err)
        })
      )
  }
}
