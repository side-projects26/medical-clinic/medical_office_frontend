import { environment } from '../../../environments/environment'
export const host: string = environment.apiUrl;
export const API_URL = {
    api_patients: `${host}/patients`,
    api_consultations: `${host}/consultations`,
    api_visitor_appointments: `${host}/visitorAppointments`,
    api_patient_appointments: `${host}/patientAppointments`,
    api_users: `${host}/users`,
    api_change_password: `${host}/users/changePassword`,
    api_infos: `${host}/infos`,
    api_database_management: `${host}/databaseManagement`,
    api_login: `${host}/login`,
    api_logo: `${host}/logo`,
    api_statistics: `${host}/statistics`,
    api_medications: `${host}/medications`,
    api_union_appointments: `${host}/union`,
    api_prescriptions: `${host}/prescriptions`,
    api_payments: `${host}/payments`,
}

