// import * as moment from "moment";
// import { format } from 'date-fns'

/**
 * convert date to YYYY/MM/DD
 * */
import { format } from 'date-fns'
export function convertDate(from) :string{
    return  format(new Date(from), "dd/MM/yyyy");
// const from=moment._i;
    console.log('from',from)
    const split=from.split('/');
    const DAY=split[2];
    const MONTH=split[1];
    const YEAR=split[0];

    const date = new Date(YEAR, MONTH, DAY);
    return  date.toLocaleDateString("fr-FR", { // you can use undefined as first argument
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
    });
    // todo look code source of format function and take its implementation then uninstall library date
  // return  `${date.date}/${date.month}/${date.year}`
 //  return  format(new Date(date), "dd/MM/yyyy") library date-fns


    // const momentDate = new Date(date);
    // return moment(momentDate).format("YYYY-MM-DD");
}
export function momentToDate(moment) :string{ // ____FunctionNotUsed
    const from=moment._i;
    const date = new Date(from.year, from.month, from.date);
    return  date.toLocaleDateString("fr-FR", { // you can use undefined as first argument
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
    })
        // dd/MM/yyyy to yyyy/MM/dd
       // .split("/").reverse().join("/");
    // todo look code source of format function and take its implementation then uninstall library date
    // return  `${date.date}/${date.month}/${date.year}`
    //  return  format(new Date(date), "dd/MM/yyyy") library date-fns


    // const momentDate = new Date(date);
    // return moment(momentDate).format("YYYY-MM-DD");
}


export function getDateToday(): string{
    return convertDate(new Date().toLocaleDateString());
}
export function stringToDate(from:string): Date{
    const split=from.split('/');
    const DAY=split[0];
    const MONTH=split[1];
    const YEAR=split[2];
    return new Date(`${YEAR}-${MONTH}-${DAY}`);
}

/*
import {DatePipe} from '@angular/common';

  constructor(private datePipe: DatePipe){}
  getTomorrow() {
    let d = new Date();
    d.setDate(d.getDate() + 1);
    return this.datePipe.transform(d, 'yyyy-MM-dd');
  }


<mat-form-field>
  <input matInput [matDatepicker]="picker" placeholder="Choose a date" [value]="getTomorrow()">
  <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
  <mat-datepicker #picker></mat-datepicker>
</mat-form-field>
  */