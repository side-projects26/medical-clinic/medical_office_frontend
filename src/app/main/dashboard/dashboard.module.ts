import {NgModule} from '@angular/core';
import {DashboardComponent} from "./dashboard.component";
import {RouterModule, Routes} from "@angular/router";
import {FuseSharedModule} from "../../../@fuse/shared.module";
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {AgmCoreModule} from '@agm/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import {FuseWidgetModule} from '@fuse/components/widget/widget.module';


const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
      //  canActivate:[AuthGuard]
    }
]

@NgModule({
    declarations: [
        DashboardComponent,
    ],
    imports: [

        RouterModule.forChild(routes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        ChartsModule,
        NgxChartsModule,

        FuseSharedModule,
        FuseWidgetModule
    ],
    exports: [
        DashboardComponent
    ]
})
export class DashboardModule {

}
