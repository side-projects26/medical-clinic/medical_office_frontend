import {TestBed} from '@angular/core/testing';

import {VisitorAppointmentService} from './visitor-appointment.service';

describe('AppointmentService', () => {
  let service: VisitorAppointmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisitorAppointmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
