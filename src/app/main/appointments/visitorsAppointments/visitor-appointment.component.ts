import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {VisitorAppointmentDto} from './visitor-appointment.model';
import {VisitorAppointmentService} from './visitor-appointment.service';
import {SelectionModel} from '@angular/cdk/collections';
import {VisitorAppointmentFormComponent} from "./visitor-appointment-form/visitor-appointment-form.component";
import {FormGroup} from "@angular/forms";
import {fuseAnimations} from "../../../../@fuse/animations";
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {Router} from "@angular/router";
import {AuthenticationService} from '../../authentication/login/authentication.service';
import {convertDate, momentToDate} from 'app/_shared/helpers';

@Component({
    selector: 'visitor-appointment',
    templateUrl: './visitor-appointment.component.html',
    styleUrls: ['./visitor-appointment.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class VisitorAppointmentComponent implements OnInit {


    private appointments_list: Array<VisitorAppointmentDto> = new Array<VisitorAppointmentDto>();
    dataSource = new MatTableDataSource<VisitorAppointmentDto>(this.appointments_list);

    selection = new SelectionModel<VisitorAppointmentDto>(true, []);
    data = Object.assign(this.appointments_list);

    @ViewChild(MatPaginator)  // {static: true}
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    public keyword: string = ''


    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    /**
      * Delete selected contacts
      */
    removeSelectedRows(): void {
        let ids: number[] = [];

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected contacts?';
        this.confirmDialogRef.afterClosed()
            .subscribe(result => {
                if (result) {
                    // this.selection.hasValue()

                    this.selection.selected.forEach(item => {
                        console.log(item.id);
                        ids.push(item.id);
                        //   let index: number = this.data.findIndex(d => d === item);
                        // console.log(this.data.findIndex(d => d === item));
                        //   this.data.splice(index, 1)
                        //   this.dataSource = new MatTableDataSource<Appointment>(this.data);
                    });
                    // this.selection = new SelectionModel<Appointment>(true, []);
                    this.appointmentService.deleteSelectedAppointments(ids)
                        .subscribe(
                            result => console.log(result),
                            error => console.error(error)
                        );
                }
                this.confirmDialogRef = null;
            });

        //////////////

        // this.selection.selected.forEach(item => {
        //   console.log(item.id);
        //   let index: number = this.data.findIndex(d => d === item);
        //   // console.log(this.data.findIndex(d => d === item));
        //   this.data.splice(index, 1)
        //   this.dataSource = new MatTableDataSource<Appointment>(this.data);
        // });
        // this.selection = new SelectionModel<Appointment>(true, []);

        //////////////







    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: VisitorAppointmentDto): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }



    constructor(private appointmentService: VisitorAppointmentService, public authService: AuthenticationService,
                public dialog: MatDialog, private _matDialog: MatDialog
        , private router: Router) {
    }

    public getDisplayedColumns(): string[] {

        if (this.authService.isNurse()) {
            return ['select', 'firstName', 'lastName', 'phone', 'date', 'time', 'status', 'Actions']
        }
        else {
            return ['firstName', 'lastName', 'phone', 'date', 'status', 'time']
        }

    }
    ngOnInit() {
        // array1.forEach(element => console.log(element));
        // this.initAppointments();
        this.getServerData(null)
    }

    initAppointments() {
        // this.dataSource.paginator = this.paginator;
        // this.appointmentService.getAppointments().subscribe(
        //   result => {
        //     console.log(result)
        //     // this.lengthTable = result.length;
        //     result.forEach(element => this.appointments_list.push(element));
        //     this.dataSource._updateChangeSubscription();

        //   }, error => {
        //     console.error(error);
        //     this.router.navigateByUrl('/auth/login'); // i must know status code == token not valid
        //   }
        // );
    }
    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.appointmentService.getAppointments(pi, ps, this.keyword).subscribe(

            response => {
                console.log(response);

                // if (response.error) {
                //     // handle error
                // } else {
                console.log(response);

                this.appointments_list = new Array<VisitorAppointmentDto>()

                response.content.forEach(element => this.appointments_list.push(element));
                this.dataSource.data = this.appointments_list


                this.dataSource._updateChangeSubscription();
                this.pageIndex = response.number;

                this.length = response.totalElements;


                this.pageSize = response.size;
                // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }
    search() {
        this.keyword = this.keyword.trim();
        this.getServerData(this.pageEvent);

    }
    dialogRef: any;
    addAppointment() {


        this.dialogRef = this._matDialog.open(VisitorAppointmentFormComponent, {
            panelClass: 'appointment-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                // console.log(response.getRawValue().dateAppointment);

                let appoi = response.getRawValue();
                
                appoi.date = convertDate(appoi.date );

                //  appoi.dateAppointment.utc(local.date);
                //  appointment.id = ++this.lengthTable;
                this.appointmentService.addAppointment(appoi)
                    .subscribe(response => {
                        //  this.getAppointments();
                        // resolve(response);
                        console.log(response)
                        this.getServerData(this.pageEvent);
                    });
            });
    }

    /**
       * Delete Appointment
       */
    deleteAppointment(id: number) {



        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.appointmentService.deleteAppointment(id)
                    .subscribe(result => {
                        console.log(result);
                        this.getServerData(this.pageEvent);
                    }, err => { console.error(err); }

                    );
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Edit AppointmentNewPatient
     *
     * @param element
     */
    editAppointment(element: VisitorAppointmentDto) {

        this.dialogRef = this._matDialog.open(VisitorAppointmentFormComponent, {
            panelClass: 'appointment-form-dialog',
            data: {
                appointment: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        let appoi = formData.getRawValue();
                
                        appoi.date = convertDate(appoi.date );
                        this.appointmentService.updateAppointment(appoi)
                            .subscribe((res) => {
                                console.log(res);
                                this.getServerData(this.pageEvent);

                            }, (err) => console.error(err)
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAppointment(formData.getRawValue().id);

                        break;
                }
            });

    }
}


