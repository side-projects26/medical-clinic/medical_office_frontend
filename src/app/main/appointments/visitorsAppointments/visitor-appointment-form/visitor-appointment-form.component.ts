import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {VisitorAppointmentDto} from "../visitor-appointment.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {stringToDate} from "../../../../_shared/helpers";

@Component({
    selector: 'app-appointment-form',
    templateUrl: './visitor-appointment-form.component.html',
    styleUrls: ['./visitor-appointment--form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VisitorAppointmentFormComponent implements OnInit {

    action: string;
    appointment: VisitorAppointmentDto;
    appointmentForm: FormGroup;
    dialogTitle: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<AppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<VisitorAppointmentFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Appointment';
            this.appointment = _data.appointment;
        }
        else {
            this.dialogTitle = 'New Appointment';
            this.appointment = new VisitorAppointmentDto({});
        }

        this.appointmentForm = this.createAppointmentForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createAppointmentForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.appointment.id],
            firstName: [this.appointment.firstName],
            lastName: [this.appointment.lastName],
            phone: [this.appointment.phone],
            // address : [this.appointment.],
            date: [stringToDate(this.appointment.date)],
            time: [this.appointment.time],
            status: [this.appointment.status]
        });
    }

    ngOnInit(): void {
    }

}
