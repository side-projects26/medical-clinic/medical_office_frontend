import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VisitorAppointmentFormComponent} from './visitor-appointment-form.component';

describe('AppointmentFormComponent', () => {
  let component: VisitorAppointmentFormComponent;
  let fixture: ComponentFixture<VisitorAppointmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorAppointmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorAppointmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
