
export class VisitorAppointmentDto {
    id:number;
    firstName: string
    lastName: string
    phone: string
    date: string
    time: string
    status: string

    /**
     * Constructor
     *
     * @param appointmentDto
     */
    constructor(appointmentDto) {
        {
           // this.id = appointmentDto.id || FuseUtils.generateGUID();
            this.firstName = appointmentDto.firstName || '';
            this.lastName = appointmentDto.lastName || '';
            this.phone = appointmentDto.phone || '';
            this.date = appointmentDto.date || '';
            this.time = appointmentDto.time || '';
            this.status = appointmentDto.status || 'Pending'
            
        }
    }
}
