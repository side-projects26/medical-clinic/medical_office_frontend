import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PatientDto} from './patient.model';
import {PatientsService} from './patients.service';
import {FuseConfirmDialogComponent} from "../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {FormGroup} from "@angular/forms";
import {PatientFormComponent} from "./patient-form/patient-form.component";
import {AuthenticationService} from '../authentication/login/authentication.service';
import {Router} from "@angular/router";
import {convertDate} from "app/_shared/helpers";
import {NotificationsService, NotificationType} from "angular2-notifications";
import {Observable, of} from "rxjs";
import {AppDataState, DataState, PageDto} from "../../_shared/_models/page.model";
import {catchError, map, startWith} from "rxjs/operators";


@Component({
    selector: 'app-patients',
    templateUrl: './patients.component.html',
    styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit
// , AfterViewInit
{
    private patients_list: Array<PatientDto> = new Array<PatientDto>();
    dataSource = new MatTableDataSource<PatientDto>(this.patients_list);
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    public keyword: string = ''

    readonly DataState = DataState;
    public dataState: DataState;

    constructor(private patientsService: PatientsService,
                public dialog: MatDialog,
                private _matDialog: MatDialog,
                public authService: AuthenticationService,
                private router: Router,
                private _notifications: NotificationsService,) {
    }


    ngOnInit() {
        this.getServerData(null)
    }

    /**
     ngAfterViewInit() {
            this.dataSource.paginator = this.paginator;
            this.getPatients()
            console.log(this.paginator)
        }
     */

    addPatient() {
        this.dialogRef = this._matDialog.open(PatientFormComponent, {
            panelClass: 'patient-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                // console.log(response.getRawValue().dateAppointment);

                let pat = response.getRawValue();
                pat.dateOfBirth = convertDate(pat.dateOfBirth);
                if (pat.dateOfBirth == 'Invalid date') {
                    pat.dateOfBirth = '';
                }
                this.patientsService.addPatient(pat)
                    .subscribe(
                        response => {
                            this.showNotificationSuccess();
                            console.log(response)
                            this.getServerData(this.pageEvent);
                        },
                        err => {
                            this.showNotificationError();
                            console.error(err)
                        }
                    );
            });
    }

    /**
     * Delete Patient
     */
    deletePatient(id: number) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.patientsService.deletePatient(id)
                    .subscribe(result => {
                            console.log(result);
                            this.getServerData(this.pageEvent);
                            this.showNotificationSuccess();
                        }, err => {
                            console.error(err);
                            this.showNotificationError();
                        }
                    );
            }
            this.confirmDialogRef = null;
        });

    }

    editPatient(element: PatientDto) {

        this.dialogRef = this._matDialog.open(PatientFormComponent, {
            panelClass: 'patient-form-dialog',
            data: {
                patient: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];

                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        const pat: PatientDto = formData.getRawValue();
                        pat.dateOfBirth = convertDate(pat.dateOfBirth);
                        if (pat.dateOfBirth == 'Invalid date') {
                            pat.dateOfBirth = '';
                        }
                        console.log(pat);
                        this.patientsService.updatePatient(pat)
                            .subscribe((res) => {
                                    console.log(res);
                                    this.getServerData(this.pageEvent);
                                    this.showNotificationSuccess()
                                }, (err) => {
                                    console.error(err)
                                    this.showNotificationError();
                                }
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deletePatient(formData.getRawValue().id);

                        break;
                }
            });
    }

    openMedicalFile(element: PatientDto) {
        this.router.navigate(['/medical-file', element.id, `${element.firstName} ${element.lastName}`]);
    }

    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;

        this.patientsService.getPatients(pi, ps, this.keyword)
            .pipe(
                map(data => {
                    console.log(data, data);
                    return {
                        dataState: DataState.LOADED, data: data
                    };
                }),
                startWith(
                    {
                        dataState: DataState.LOADING, data: null
                    }
                ),
                catchError(err => of({
                    dataState: DataState.ERROR, errorMessage: err.message, data: null
                })))
            .subscribe(
                response => {
                    console.log('response fin', response)


                    // if (response.error) {
                    //     // handle error
                    // } else {

                    this.dataState=response.dataState;
                    if (this.dataState == DataState.LOADED) {
                        this.patients_list = new Array<PatientDto>()
                        response.data.content.forEach(element => this.patients_list.push(element));
                        this.dataSource.data = this.patients_list
                        this.dataSource._updateChangeSubscription();
                        this.pageIndex = response.data.number;
                        this.length = response.data.totalElements;
                        this.pageSize = response.data.size;
                    }
                }
                /*,
                error => {
                    // handle error
                }*/
            );
        return event;
    }

    search() {
        this.keyword = this.keyword.trim();
        this.getServerData(this.pageEvent);

    }

    public getDisplayedColumns(): string[] {
        return ['firstName', 'lastName', 'phone', 'dateCreation', 'Actions'];
    }

    private showNotificationSuccess(): void {
        this._notifications.create('Success', 'content', NotificationType.Success)
    }

    private showNotificationError(): void {
        this._notifications.create('Error', 'content', NotificationType.Error,
            {
                timeOut: 3000,
            })
    }
}
