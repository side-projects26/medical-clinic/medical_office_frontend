import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PatientDto} from "./patient.model";
import {API_URL} from "app/_shared/config/api.url.config";
import {PageDto} from 'app/_shared/_models/page.model';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

    private urlResource:string=API_URL.api_patients;
    constructor(private http:HttpClient) { }

    getPatients(pageIndex: number,pageSize: number,
        keyword: string
        ):Observable<PageDto<PatientDto>>{
        // return this.http.get<Patient[]>(`${this.urlResource}?page=${pageIndex}&limit=${pageSize}`)
        return this.http.get<PageDto<PatientDto>>(this.urlResource,
            {
                params: new HttpParams()
                    .set('page',pageIndex.toString())
                    .set('limit',pageSize.toString())
                    .set('keyword',keyword)

            })
    }
    getOnePatient(id:number):Observable<PatientDto>{
        return this.http.get<PatientDto>(`${this.urlResource}/${id}`)
    }

    deletePatient(id:number) :Observable<PatientDto>{
        return this.http.delete<PatientDto>(this.urlResource+`/${id}`);
    }
    addPatient(patient:PatientDto) {
        return  this.http.post(this.urlResource,patient)
    }
    updatePatient(patient:PatientDto) {
        return  this.http.put(this.urlResource+`/${patient.id}`,patient)
    }

    

}

