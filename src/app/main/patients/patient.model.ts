export class PatientDto {
    id: number;
    firstName: string
    lastName: string
    phone: string
    dateCreation: string;
    address: string;
    profession: string;
    dateOfBirth: string;
    gender: string;
    // medicalHistory
    allergies: string;
    medications: string;
    oldDiseases: string;
    currentDiseases: string;


    /**
     * Constructor
     *
     * @param patientDto
     */
    constructor(patientDto) {
        {
            // this.id = appointment.id || FuseUtils.generateGUID();
            this.firstName = patientDto.firstName || '';
            this.lastName = patientDto.lastName || '';
            this.phone = patientDto.phone || '';
            this.dateCreation = patientDto.dateCreation || '';
            this.address = patientDto.address || '';
            this.profession = patientDto.profession || '';
            this.dateOfBirth = patientDto.dateOfBirth || '';
            this.gender = patientDto.gender || null;

            this.allergies = patientDto.allergies|| '';
            this.medications = patientDto.medications|| '';
            this.oldDiseases = patientDto.oldDiseases|| '';
            this.currentDiseases = patientDto.currentDiseases|| '';


        }
    }
}
