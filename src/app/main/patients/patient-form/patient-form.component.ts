import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PatientDto} from "../patient.model";
import {stringToDate} from "../../../_shared/helpers";

@Component({
    selector: 'app-patient-form',
    templateUrl: './patient-form.component.html',
    styleUrls: ['./patient-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PatientFormComponent implements OnInit {
    action: string;
    patient: PatientDto;
    patientForm: FormGroup;
    dialogTitle: string;


    /**
     * Constructor
     *
     * @param {MatDialogRef<AppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<PatientFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Patient';
            this.patient = _data.patient;
        } else {
            this.dialogTitle = 'New Patient';
            this.patient = new PatientDto({});
        }

        this.patientForm = this.createPatientForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createPatientForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.patient.id],
            firstName: [this.patient.firstName],
            lastName: [this.patient.lastName],
            phone: [this.patient.phone],
            gender: [this.patient.gender],
            dateOfBirth: [stringToDate(this.patient.dateOfBirth)],
            address: [this.patient.address],
            profession: [this.patient.profession],
            allergies: [this.patient.allergies],
            medications: [this.patient.medications],
            oldDiseases: [this.patient.oldDiseases],
            currentDiseases: [this.patient.currentDiseases]


        });
    }

    ngOnInit(): void {
    }


    //   form: FormGroup
    //   /**
    //    * Constructor
    //    *
    //    * @param {FormBuilder} _formBuilder
    //    */
    //   constructor(
    //       private _formBuilder: FormBuilder
    //   ){
    //   }
    //
    // ngOnInit(): void {
    //     // Reactive Form
    //     this.form = this._formBuilder.group({
    //         company   : [
    //             {
    //                 value   : 'Google',
    //                 disabled: true
    //             }, Validators.required
    //         ],
    //         firstName : ['', Validators.required],
    //         lastName  : ['', Validators.required],
    //         address   : ['', Validators.required],
    //         address2  : ['', Validators.required],
    //         city      : ['', Validators.required],
    //         state     : ['', Validators.required],
    //         postalCode: ['', [Validators.required, Validators.maxLength(5)]],
    //         country   : ['', Validators.required]
    //     });
    // }

}
