import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientsComponent} from "./patients.component";
import {RouterModule, Routes} from "@angular/router";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';

import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FuseSharedModule} from "../../../@fuse/shared.module";
import {PatientFormComponent} from './patient-form/patient-form.component';
import {MatSelectModule} from "@angular/material/select";
import {SimpleNotificationsModule} from "angular2-notifications";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


const routes: Routes = [
    {
        path: '',
        component: PatientsComponent
    }
]

@NgModule({
    declarations: [
        PatientsComponent,
        PatientFormComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatPaginatorModule,
        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatSelectModule,
        MatProgressSpinnerModule,


    ]
})
export class PatientsModule {
}
