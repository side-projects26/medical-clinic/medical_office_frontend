import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-consultations-appointments-per',
  templateUrl: './medical-file.component.html',
  styleUrls: ['./medical-file.component.scss']
})
export class MedicalFileComponent implements OnInit {
    @Input() patientId: number;
    public fullName: string;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
      this.patientId = parseInt(this.route.snapshot.paramMap.get('patientId'));
      this.fullName=this.route.snapshot.paramMap.get('fullname')
  }

}
