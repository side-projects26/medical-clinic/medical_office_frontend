import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientAppointmentComponent} from './patient-appointments/patient-appointment.component';
import {Route, RouterModule} from "@angular/router";
import {ConsultationsComponent} from "./consultations/consultations.component";
import {ConsultationFormComponent} from "./consultations/consultation-form/consultation-form.component";
import {FuseSharedModule} from "../../../../@fuse/shared.module";
import {MatCardModule} from "@angular/material/card";
import {FlexModule} from "@angular/flex-layout";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {FuseWidgetModule} from "../../../../@fuse/components";
import {MedicalFileComponent} from './medical-file.component';
import {PatientAppointmentFormComponent} from './patient-appointments/patient-appointment-form/patient-appointment-form.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatSelectModule} from "@angular/material/select";


const routes:Route[]=[
    {
        path: 'medical-file/:patientId/:fullname',
        //  path: 'consultations',
        // data:{patinet:Patient},
        component: MedicalFileComponent
    }
]
@NgModule({
  declarations: [
      PatientAppointmentComponent,
      ConsultationsComponent,
      ConsultationFormComponent,
      MedicalFileComponent,
      PatientAppointmentFormComponent,


  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FuseSharedModule,
        MatCardModule,
        FlexModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatToolbarModule,
        MatDialogModule,
        MatDatepickerModule,
        FuseWidgetModule, CommonModule,
        RouterModule.forChild(routes),
        FuseSharedModule,
        MatCardModule,
        FlexModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatToolbarModule,
        MatDialogModule,
        MatDatepickerModule,
        FuseWidgetModule, MatTooltipModule,
        MatSelectModule,
    ]
})
export class Medical_fileModule { }
