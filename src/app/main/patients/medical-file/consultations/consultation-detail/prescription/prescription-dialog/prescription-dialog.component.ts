import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PrescriptionDto} from "./prescription.model";
import {getDateToday, stringToDate} from "../../../../../../../_shared/helpers";

@Component({
    selector: 'app-prescription-dialog',
    templateUrl: './prescription-dialog.component.html',
    styleUrls: ['./prescription-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PrescriptionDialogComponent implements OnInit {

    action: string;
    prescriptionDto: PrescriptionDto;
    form: FormGroup;
    dialogTitle: string;

    constructor(
        public matDialogRef: MatDialogRef<PrescriptionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit Prescription') {
            this.dialogTitle = 'Edit Prescription';
            this.prescriptionDto = _data.prescriptionDto;
        } else if (this.action === 'create Prescription') {
            this.dialogTitle = 'Create Prescription';
            this.prescriptionDto = new PrescriptionDto({});
            this.prescriptionDto.date = getDateToday()
        }

        // if (!_data.prescriptionDto) {
        //     this.prescriptionDto = _data.prescriptionDto;
        // } else {

        //}


        this.form = this.createForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createForm(): FormGroup {
        return this._formBuilder.group({

            date: [stringToDate(this.prescriptionDto.date)],
            detail: [this.prescriptionDto.detail, [Validators.required]]

        });
    }

    ngOnInit(): void {
    }

}
