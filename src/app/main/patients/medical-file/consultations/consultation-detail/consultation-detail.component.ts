import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {fuseAnimations} from "../../../../../../@fuse/animations";
import {FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {PrescriptionDialogComponent} from "./prescription/prescription-dialog/prescription-dialog.component";
import {PrescriptionService} from "./prescription/prescription.service";
import {convertDate} from "../../../../../_shared/helpers";

@Component({
    selector: 'app-consultation-detail',
    templateUrl: './consultation-detail.component.html',
    styleUrls: ['./consultation-detail.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class ConsultationDetailComponent implements OnInit {
    public consultationId: number;
    public patientId: number;
    public fullname: string;
    dialogRef: any;
    label_btn_prescription: string = '';

    constructor(private route: ActivatedRoute, private _matDialog: MatDialog,
                private prescriptionService: PrescriptionService) {
    }

    ngOnInit(): void {
        this.consultationId = parseInt(this.route.snapshot.paramMap.get('consId'));
        this.patientId = parseInt(this.route.snapshot.paramMap.get('patientId'));

        this.fullname = this.route.snapshot.paramMap.get('fullname');
this.updateLabelBtn_prescription();
    }

    updateLabelBtn_prescription() {
        this.prescriptionService.isAlreadyExist(this.consultationId)
            .subscribe(res => {
                this.label_btn_prescription = res ? 'edit Prescription' : 'create Prescription'
            })
    }

    initPrescription() {
        let element = null;
        if (this.label_btn_prescription == 'create Prescription') {
            this.openPrescription(element);
        } else if (this.label_btn_prescription == 'edit Prescription') {
            this.prescriptionService.get(this.consultationId)
                .subscribe(res => {
                        element = res;

                    },
                    err => {
                        console.log(err)

                    },
                    () => {
                        this.openPrescription(element);
                    })
        }


    }

    openPrescription(element) {


        this.dialogRef = this._matDialog.open(PrescriptionDialogComponent, {
            panelClass: 'prescription-form-dialog',
            data: {
                prescriptionDto: element,
                action: this.label_btn_prescription
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                console.log(response);
                const action = response[0];

                if (action == 'save') {
                    let prescription = response[1].getRawValue();
                    prescription.date = convertDate(prescription.date);
                    if (prescription.date == 'Invalid date') {
                        prescription.date = '';
                    }
                    this.prescriptionService.save(this.consultationId, prescription)
                        .subscribe(
                            response => {
                                //this.showNotificationSuccess();
                                console.log(response);
                                this.updateLabelBtn_prescription();


                            },
                            err => {
                                //  this.showNotificationError();
                                console.error(err)
                            });


                } else if (action == 'print') {
                    let prescription = response[1].getRawValue();
                    prescription.date = convertDate(prescription.date);
                    if (prescription.date == 'Invalid date') {
                        prescription.date = '';
                    }

                    this.prescriptionService.print(prescription.detail, prescription.date)
                }
            });
    }

    openCertificate() {

    }
}
