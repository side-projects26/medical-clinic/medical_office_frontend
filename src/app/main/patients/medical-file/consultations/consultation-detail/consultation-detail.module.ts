import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConsultationDetailComponent} from "./consultation-detail.component";
import {RouterModule, Routes} from "@angular/router";

import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {FuseSharedModule} from "../../../../../../@fuse/shared.module";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {CertificateComponent} from './tabs/certificate/certificate.component';
import {InfosPatientComponent} from './tabs/infos-patient/infos-patient.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

import {FileUploadModule} from 'ng2-file-upload';

import {MatDialogModule} from "@angular/material/dialog";
import {GalleryModule} from '@ks89/angular-modal-gallery';
import {MatListModule} from '@angular/material/list';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ReactiveFormsModule} from "@angular/forms";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {MatGridListModule} from "@angular/material/grid-list";
import { PrescriptionDialogComponent } from './prescription/prescription-dialog/prescription-dialog.component';
import {FuseWidgetModule} from "../../../../../../@fuse/components";

const routes: Routes = [

    {
        path: 'consultationDetail/:consId/:patientId/:fullname',
        component: ConsultationDetailComponent,

        //  canActivate:[AuthGuard]
    }
]

@NgModule({
    declarations: [
        ConsultationDetailComponent,
        CertificateComponent,
        InfosPatientComponent,
        PrescriptionDialogComponent,

    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),

        FuseSharedModule,
        MatTabsModule,

        MatFormFieldModule,
        MatInputModule,
        FileUploadModule,
        MatDialogModule,
        MatListModule,
        MatPaginatorModule,
        // MatTableDataSource,
        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,

        MatDatepickerModule,
        MatSelectModule,
        MatPaginatorModule,
        // MatTableDataSource,
        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatSelectModule,


        GalleryModule,
        MatGridListModule,
        FuseWidgetModule


    ]
})
export class ConsultationDetailModule {
}
