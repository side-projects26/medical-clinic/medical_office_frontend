export class ConsultationDto {
    id: number;
    date: string;
    reason: string;
    payment: {
        id: number;
        total: number;
        amountPaid: number;
        status: string;

    }


    /**
     * Constructor
     *
     * @param consultation
     */
    constructor(consultation) {
        {
            // this.id = appointment.id || FuseUtils.generateGUID();

            this.date = consultation.date || '';
            this.reason = consultation.reason || '';
            this.payment = consultation.payment || {
                total: 0,
                amountPaid: 0,
                status: ''
            };

        }
    }
}
