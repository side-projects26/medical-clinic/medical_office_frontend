import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from "../../../authentication/login/authentication.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {PatientAppointmentService} from "./patient-appointment.service";
import {MatTableDataSource} from "@angular/material/table";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {FormGroup} from "@angular/forms";
import {PatientAppointmentFormComponent} from "./patient-appointment-form/patient-appointment-form.component";
import {PatientAppointmentDto} from "./patient-appointment.model";
import {convertDate} from 'app/_shared/helpers'

@Component({
    selector: 'patient-appointment',
    templateUrl: './patient-appointment.component.html',
    styleUrls: ['./patient-appointment.component.scss']
})
export class PatientAppointmentComponent implements OnInit {

    @Input() patientId: number;


    private appointmentsPer_list: Array<PatientAppointmentDto> = new Array<PatientAppointmentDto>();
    dataSource = new MatTableDataSource<PatientAppointmentDto>(this.appointmentsPer_list);
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    
    
    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    public keyword: string=''



    constructor(public authService: AuthenticationService,
                public dialog: MatDialog,
                private _matDialog: MatDialog,
                private appPerService: PatientAppointmentService) {
    }

    ngOnInit(): void {
        this.getServerData(null);
/* 
        this.dataSource.paginator = this.paginator;
        this.appPerService.getAppointmentsPersonal(this.patientId).subscribe(
            result => {
                console.log(result)
                result.forEach(element => this.appointmentsPer_list.push(element));
                this.dataSource._updateChangeSubscription();

            }, error => console.error(error)
        );
  */
    }

    public getServerData(event?: PageEvent) {
        this.pageEvent=event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.appPerService.getAppointments(this.patientId,pi, ps,this.keyword).subscribe(

            response => {
                console.log(response);
                
                // if (response.error) {
                //     // handle error
                // } else {
                    this.appointmentsPer_list = new Array<PatientAppointmentDto>()

                    response.content.forEach(element => this.appointmentsPer_list.push(element));
                    this.dataSource.data = this.appointmentsPer_list
                    

                    this.dataSource._updateChangeSubscription();
                    this.pageIndex = response.number;
                    this.length = response.totalElements;
                    this.pageSize = response.size;
               // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }
    search() {
        this.keyword=this.keyword.trim();
        this.getServerData(this.pageEvent);

    }

    addAppointmentPatient() {
        this.dialogRef = this._matDialog.open(PatientAppointmentFormComponent, {
            panelClass: 'AppointmentPatient-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                // console.log(response.getRawValue().dateAppointment);

                let appoi = response.getRawValue();
                appoi.date=convertDate(appoi.date)
                console.log(appoi)

                //  appoi.dateAppointment.utc(local.date);
                //  appointment.id = ++this.lengthTable;
                this.appPerService.addAppointment(this.patientId, appoi)
                    .subscribe(
                        response => {
                            this.getServerData(this.pageEvent)
                            console.log(response)},
                        err => console.error(err)
                    );
            });
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    deleteAppointment(id: number) {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.appPerService.deleteAppointment(id)
                    .subscribe(result => {
                            console.log(result);
                            this.getServerData(this.pageEvent);
                        }, err => {
                            console.error(err);
                        }
                    );
            }
            this.confirmDialogRef = null;
        });
    }

    editAppointment(element: any) {
        this.dialogRef = this._matDialog.open(PatientAppointmentFormComponent, {
            panelClass: 'AppointmentPatient-form-dialog',
            data: {
                appointmentPersonal: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                         let rawValue=formData.getRawValue();
                         rawValue.date=convertDate(rawValue.date);
                        this.appPerService.updateAppointment(rawValue)
                            .subscribe((res) => {
                                    console.log(res);
                                    this.getServerData(this.pageEvent)
                                }, (err) => console.error(err)
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteAppointment(formData.getRawValue().id);

                        break;
                }
            });
    }

    public getDisplayedColumns(): string[] {
         if (this.authService.isNurse()){
            return ['date', 'time','status','Actions'];
        }else {
            return ['date', 'time','status'];
        }

    }
}
