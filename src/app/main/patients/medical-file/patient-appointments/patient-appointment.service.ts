import {Injectable} from '@angular/core';
import {API_URL} from "app/_shared/config/api.url.config";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PatientAppointmentDto} from "./patient-appointment.model";
import {ResponseModel} from "../../../appointments/patientsAppointments/response.model";
import {PageDto} from 'app/_shared/_models/page.model';
import {getDateToday} from 'app/_shared/helpers';

@Injectable({
  providedIn: 'root'
})
export class PatientAppointmentService {

    private urlResource:string=API_URL.api_patient_appointments;
    constructor(private http:HttpClient) { }

    getAppointments(patientId: number, pageIndex: number, pageSize: number,
                    keyword: string) :Observable<PageDto<PatientAppointmentDto>>{
        return this.http.get<PageDto<PatientAppointmentDto>>(`${this.urlResource}/${patientId}`,
        {
            params: new HttpParams()
                .set('page',pageIndex.toString())
                .set('limit',pageSize.toString())
                .set('keyword',keyword)

        })
    }
    deleteAppointment(id:number) :Observable<PatientAppointmentDto>{
        return this.http.delete<PatientAppointmentDto>(this.urlResource+`/${id}`);
    }
    addAppointment(patientId:number, appointment:PatientAppointmentDto) {
        
        // console.log(appointment)
        return  this.http.post(`${this.urlResource}/${patientId}`,appointment)
    }
    updateAppointment(appointment:PatientAppointmentDto) {
        return  this.http.put(this.urlResource+`/${appointment.id}`,appointment)
    }
/*   version 1 without pagination
    getOldAppointmentsOfAllPatients() :Observable<ResponseModel[]>{
        return this.http.get<ResponseModel[]>(`${this.urlResource}/allPending`)
    }
    */
//    version 2 pagination
    getAllPatientsAppointments(pageIndex: number, pageSize: number, date:string) :Observable<PageDto<ResponseModel>>{
        if(date=='Invalid date'){
            date=getDateToday()
        }
       
        
        return this.http.get<PageDto<ResponseModel>>(`${this.urlResource}/allPending`,
        {

            params: new HttpParams()
                .set('page',pageIndex.toString())
                .set('limit',pageSize.toString())
                .set('date',date)

        }
        )
    }
    
       
}
