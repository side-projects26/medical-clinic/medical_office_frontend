import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PatientAppointmentDto} from "../patient-appointment.model";
import {stringToDate} from "../../../../../_shared/helpers";

@Component({
  selector: 'appointment-patient-form',
  templateUrl: './patient-appointment-form.component.html',
  styleUrls: ['./patient-appointment-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PatientAppointmentFormComponent implements OnInit {


    action: string;
    appointmentPatient: PatientAppointmentDto;
    form: FormGroup;
    dialogTitle: string;


    /**
     * Constructor
     *
     * @param {MatDialogRef<PatientAppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<PatientAppointmentFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Appointment';
            this.appointmentPatient = _data.appointmentPersonal;
        }
        else {
            this.dialogTitle = 'New Appointment';
            this.appointmentPatient = new PatientAppointmentDto({});
        }

        this.form = this.createForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.appointmentPatient.id],
            date: [stringToDate(this.appointmentPatient.date)],
            time: [this.appointmentPatient.time],
            status: [this.appointmentPatient.status]
        });
    }

    ngOnInit(): void {
    }


}
