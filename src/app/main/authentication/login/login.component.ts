import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {AuthenticationService} from "./authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NavigationService} from "../../../navigation/navigation";
import {catchError, switchMap} from 'rxjs/operators';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    bad_credentials: boolean = false;
    @ViewChild('signInNgForm') signInNgForm: NgForm;
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param authService
     * @param router
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authService: AuthenticationService,
        private router: Router,
        private navigationService: NavigationService,
        private _activatedRoute: ActivatedRoute,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            username: ['', [Validators.required, Validators.minLength(4)]],
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
        this.loginForm.patchValue({
            username: 'admin',
            password: '12345678',
        });
    }

    onLogin() {
        // console.log(this.loginForm.getRawValue())
// Return if the form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        // Disable the form
        this.loginForm.disable();

        // Hide the alert
        this.bad_credentials = false;

        // Sign in
        this.authService.signIn(this.loginForm.getRawValue())
            .subscribe(
                () => {

                    // Set the redirect url.
                    // The '/signed-in-redirect' is a dummy url to catch the request and redirect the user
                    // to the correct page after a successful sign in. This way, that url can be set via
                    // routing file and we don't have to touch here.
                    const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/signed-in-redirect';

                    // Navigate to the redirect url
                    this.router.navigateByUrl(redirectURL);

                },
                (response) => {

                    // Re-enable the form
                    this.loginForm.enable();

                    // Reset the form
                    this.signInNgForm.resetForm();

                    // Show the alert
                    this.bad_credentials=true
                }
            );
        /*
                this.authService.login(this.loginForm.getRawValue())
                    .subscribe(
                     resp=>{
                         this.bad_credentials=false;
                         let jwt=resp.headers.get('Authorization');
                         this.authService.saveToken(jwt);
                         this.navigationService.updateNavigationSideMenu()
                         this.router.navigateByUrl('/dashboard');
                     },
                        error => {
                            console.log(error);
                            this.bad_credentials=true;

                        }
                    )
                    */
    }
}
