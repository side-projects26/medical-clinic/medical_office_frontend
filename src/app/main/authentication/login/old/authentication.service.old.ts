import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { API_URL } from "app/_shared/config/api.url.config";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AuthenticationServiceOld {
    private urlResource: string = API_URL.api_login;
    private jwtToken: string;
    private roles: Array<any> = [];

    constructor(private http: HttpClient, private router: Router
        , private jwtHelper: JwtHelperService) {
    }

    login(user) {
        return this.http.post(this.urlResource, user, { observe: 'response' })
    }

    saveToken(jwt: string) {
        this.jwtToken = jwt;
        localStorage.setItem("token", this.jwtToken);
        //let jwtHelper = new JwtHelperService();
        this.roles = this.jwtHelper.decodeToken(this.jwtToken) // acceder au claims
            .roles;
    }
    getToken() {
        return localStorage.getItem('token');
    }


    logout() {
        this.jwtToken = null;
        localStorage.removeItem('token');
        this.router.navigateByUrl('/auth/login')
    }

    // isValid(){
    //     const token=this.getToken();  idbrahim
    //     if (token){

    //     }
    // }
    isAuthenticated(): boolean {
        const token = this.getToken();
        /// let jwtHelper = new JwtHelperService();
        return !this.jwtHelper.isTokenExpired(token);
    }
    isAdmin() {
        for (let r of this.roles) {
            if (r.authority == 'ADMIN')
                return true;
        }
        return false;
    }

    isDoctor() {
        for (let r of this.roles) {
            if (r.authority == 'DOCTOR')
                return true;
        }
        return false;
    }
    isNurse() {

        //let jwtHelper = new JwtHelperService();
        this.roles = this.jwtHelper.decodeToken(localStorage.getItem("token")) // acceder au claims
            .roles;

        //console.log('roles',this.roles);

        for (let r of this.roles) {
            if (r.authority == 'NURSE')
                return true;
        }
        return false;
    }

    getUsernameFromToken(): string {
        return this.jwtHelper.decodeToken(localStorage.getItem("token")) // acceder au claims
            ?.sub;
    }
    getRole(): string {
        return this.jwtHelper.decodeToken(localStorage.getItem("token")) // acceder au claims
            ?.roles[0].authority;
    }
}
