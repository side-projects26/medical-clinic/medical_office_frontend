import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {KEY_TOKEN} from "../../../_shared/constantes";
import {AuthUtils} from "../../../_shared/auth.utils";
import {API_URL} from "../../../_shared/config/api.url.config";
import {JwtHelperService} from "@auth0/angular-jwt";


@Injectable({providedIn: 'root'})
export class AuthenticationService {
    private _authenticated: boolean = false;
    private urlResource: string = API_URL.api_login;

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        // private _userService: UserService
        private jwtHelper: JwtHelperService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set accessToken(token: string) {
        localStorage.setItem(KEY_TOKEN, token);
    }

    get accessToken(): string {
         return localStorage.getItem(KEY_TOKEN) ?? '';
        //return 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTYyMzM1NjM5Niwicm9sZXMiOlt7ImF1dGhvcml0eSI6IkFETUlOIn0seyJhdXRob3JpdHkiOiJET0NUT1IifSx7ImF1dGhvcml0eSI6IlNFQ1JFVEFSWSJ9XX0.DOIEbdfLBBu-gCdRA88WKPlZDIY8UPbv6Guo8OGZtw5FtYQisofxZuGCFqtcJhnTHTiViZYqnQYCmf4kXu7I-w';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    /**
     * Sign in
     *
     * @param user
     */

    signIn(user): Observable<any> {
        // Throw error, if the user is already logged in
        if (this._authenticated) {
            return throwError('User is already logged in.');
        }

        return this._httpClient.post(this.urlResource, user, {observe: 'response'}).pipe(
            switchMap((response: any) => {

                // Store the access token in the local storage
                this.accessToken = response.headers.get('Authorization');


                // Set the authenticated flag to true
                this._authenticated = true;

                // Store the user on the user service
                //this._userService.user = response.user;

                // Return a new observable with the response
                return of(response);
            })
        );
    }

    /**
     * Sign in using the access token
     */

    /*
    signInUsingToken(): Observable<any> {
        // Renew token
        return this._httpClient.post('api/auth/refresh-access-token', {
            accessToken: this.accessToken
        }).pipe(
            catchError(() =>

                // Return false
                of(false)
            ),
            switchMap((response: any) => {

                // Store the access token in the local storage
                this.accessToken = response.accessToken;

                // Set the authenticated flag to true
                this._authenticated = true;

                // Store the user on the user service
                this._userService.user = response.user;

                // Return true
                return of(true);
            })
        );
    }
     */
    /**
     * Sign out
     */
    signOut(): Observable<any> {
        // Remove the access token from the local storage
        localStorage.removeItem(KEY_TOKEN);

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }


    /**
     * Unlock session
     *
     * @param credentials
     */

    /*
    unlockSession(credentials: { email: string; password: string }): Observable<any> {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }
*/
    /**
     * Check the authentication status
     */
    check(): Observable<boolean> {
        // Check if the user is logged in
        if (this._authenticated) {
            return of(true);
        }

        // Check the access token availability
        if (!this.accessToken) {
            return of(false);
        }

        // Check the access token expire date
        if (AuthUtils.isTokenExpired(this.accessToken)) {
            return of(false);
        }

        // If the access token exists and it didn't expire, sign in using it

        // return this.signInUsingToken();
        return of(false); // todo
    }


    getUsernameFromToken(): string {
        return this.jwtHelper.decodeToken(this.accessToken) // acceder au claims
            ?.sub;

    }

    getRole(): string {
        return this.jwtHelper.decodeToken(this.accessToken) // acceder au claims
            ?.roles[0].authority;
        //return 'Role'
    }

    isAdmin() {
        return this.getRole() == 'ADMIN';
    }

    isDoctor() {
        return this.getRole() == 'DOCTOR'|| this.isAdmin();
    }

    isNurse() {
        return this.getRole() == 'NURSE' || this.isAdmin();
    }
}
