import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from "../../../@fuse/animations";
import {MedicationDto} from "../settings/medications/medication.model";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MedicationsService} from "../settings/medications/medications.service";
import {AuthenticationService} from "../authentication/login/authentication.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {NotificationsService, NotificationType} from "angular2-notifications";
import {FuseConfirmDialogComponent} from "../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MedicationFormComponent} from "../settings/medications/medication-form/medication-form.component";
import {FormGroup} from "@angular/forms";
import {ResponseAllPayments} from "./responseAllPayments.model";
import {PaymentsService} from "./payments.service";

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PaymentsComponent implements OnInit {

    private payments_list: Array<ResponseAllPayments> = new Array<ResponseAllPayments>();
    dataSource = new MatTableDataSource<ResponseAllPayments>(this.payments_list);

    selection = new SelectionModel<ResponseAllPayments>(true, []);
    data = Object.assign(this.payments_list);

    @ViewChild(MatPaginator)  // {static: true}
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;

    keyword: string = '';

    constructor(private paymentsService: PaymentsService, public authService: AuthenticationService,

         private router: Router,
                private _notifications: NotificationsService,) {
    }





    



    public getDisplayedColumns(): string[] {

        return [  'firstName','lastName','consultationDate','amount_paid','total','status','Actions']


    }

    ngOnInit() {
        this.getServerData(null)
    }



    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.paymentsService.getPayments(pi, ps).subscribe(
            response => {
                console.log('payments',response);

                // if (response.error) {
                //     // handle error
                // } 
                // else {
                this.payments_list = new Array<ResponseAllPayments>()

                response.content.forEach(element => this.payments_list.push(element));
                this.dataSource.data = this.payments_list


                this.dataSource._updateChangeSubscription();


                this.pageIndex = response.number;

                this.length = response.totalElements;


                this.pageSize = response.size;
                // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }




    goToConsultationOfPayment(element) {
        this.router.navigate(['/medical-file', element.consultationId, `${element.firstName} ${element.lastName}`]);

    }
}
