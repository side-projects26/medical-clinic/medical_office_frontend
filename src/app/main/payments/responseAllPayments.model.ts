export class ResponseAllPayments {

    consultationId: number;
    firstName: string;
    lastName: string;

    consultationDate: string;
    amount_paid: string;

    total: string;

    status: string;
}