export class InfosDto {
    id: number;
    logoBase64: string;
    medicalOfficeName: string;
    phone: string;
    doctorDto: {
        name: string;
        specialty: string
    };

    address: string;
    timeZone: string;


}