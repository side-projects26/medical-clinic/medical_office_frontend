import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {InfosDto} from "../infos.model";
import {InfosService} from "../infos.service";

@Component({
    selector: 'app-infos-form',
    templateUrl: './infos-form.component.html',
    styleUrls: ['./infos-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InfosFormComponent implements OnInit {
    dialogTitle: string;
    action: string;
    infos: InfosDto;
    infosForm: FormGroup;
    public listTimeZones: String[];

    constructor(public matDialogRef: MatDialogRef<InfosFormComponent>, private infosService: InfosService,
                @Inject(MAT_DIALOG_DATA) private _data: any,
                private _formBuilder: FormBuilder) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Infos';
            this.infos = _data.infos;

        }
        // // Set the private defaults
        // this._unsubscribeAll = new Subject();

        this.infosService.getListTimeZones()
            .subscribe(res => {
                this.listTimeZones = res;
            }, err => console.log(err));

        this.infosForm = this.createForm()
    }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    createForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.infos.id],
            medicalOfficeName: [this.infos.medicalOfficeName],
            address: [this.infos.address],
            timeZone: [this.infos.timeZone],
            doctorDto: this._formBuilder.group(
                {
                    name: [this.infos.doctorDto.name],
                    specialty: [this.infos.doctorDto.specialty],
                }
            ),
            phone: [this.infos.phone]
        });

    }

    ngOnInit(): void {
    }

}
