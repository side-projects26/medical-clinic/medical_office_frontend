import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FileUploader} from 'ng2-file-upload';
import {AuthenticationService} from 'app/main/authentication/login/authentication.service';
import {API_URL} from "app/_shared/config/api.url.config";

@Component({
  selector: 'app-logo-form',
  templateUrl: './logo-form.component.html',
  styleUrls: ['./logo-form.component.scss']
})
export class LogoFormComponent implements OnInit {
    uploader: FileUploader;
    hasBaseDropZoneOver: boolean;
    hasAnotherDropZoneOver: boolean;

    @ViewChild('fileInput') fileInput: ElementRef;
    isDropOver: boolean;

    private api_logo: string= API_URL.api_logo;

    constructor(public matDialogRef: MatDialogRef<LogoFormComponent>,
        @Inject(MAT_DIALOG_DATA) public dialogData: any,
                private authService: AuthenticationService,
                ) {


        this.uploader = new FileUploader({
            url: `${this.api_logo}/${dialogData.logoId}`,  //patient id
            method: 'PUT',
            additionalParameter: {
                con_id: dialogData.cons_id    // consultaion id
            },
            itemAlias: 'logo',
            authTokenHeader: 'Authorization',
            authToken: this.authService.accessToken, //getgetToken(),
        });


        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;

       // this.response = '';

        this.uploader.response.subscribe(res => {
                console.log(res)
        },
            (err)=>{
                console.log(err)
            }
            );

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

    }

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
        this.isDropOver = e;
    }

    ngOnInit(): void {
    }

    fileClicked() {
        this.fileInput.nativeElement.click();
    }

    OnClose() {
        const uploadSucceeded: boolean = this.uploader.queue.length > 0;
        this.matDialogRef.close({uploadSucceeded: uploadSucceeded});
    }



    // dialogTitle: string;
    // action: string;

    // logoForm: FormGroup;
    // constructor(public matDialogRef: MatDialogRef<LogoFormComponent>,
    //             @Inject(MAT_DIALOG_DATA) private _data: any,
    //             private _formBuilder: FormBuilder) {
    //     // Set the defaults
    //     this.action = _data.action;

    //     if (this.action === 'edit') {
    //         this.dialogTitle = 'Edit Logo';


    //     }
    //     // // Set the private defaults
    //     // this._unsubscribeAll = new Subject();

    //     this.logoForm = this.createForm()
    // }
    // /**
    //  * Create user form
    //  *
    //  * @returns {FormGroup}
    //  */
    // createForm(): FormGroup {
    //     return this._formBuilder.group({
    //         logo: new FormControl('',[Validators.required]),
    //     });

    // }
    // ngOnInit(): void {
    // }

    // onFileChange($event) {
    //     this.logoForm.patchValue(
    //         {
    //             logo:<File>$event.target.files[0]
    //         }
    //     )
    // }
}
