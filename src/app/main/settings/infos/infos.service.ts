import {Injectable} from '@angular/core';
import {API_URL} from "app/_shared/config/api.url.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {InfosDto} from "./infos.model";

@Injectable({
  providedIn: 'root'
})
export class InfosService {

    private urlResource:string=API_URL.api_infos;
    private api_logo: string= API_URL.api_logo;
    constructor(private http:HttpClient) {
    }

    getInfos():Observable<InfosDto>{
        return this.http.get<InfosDto>(this.urlResource)
    }
    // getRole(url:string):Observable<any>{
    //   return this.http.get(url);
    // }
    // deleteInfos(id:number) :Observable<UserResponse>{
    //     return this.http.delete<UserResponse>(this.urlResource+`/${id}`);
    // }
    // addUser(user:UserRequest) {
    //     return  this.http.post(this.urlResource,user)
    // }
    updateInfos(infos:InfosDto) {
        return  this.http.put(this.urlResource,infos)
    }

    updateLogo(file:File,id: number) {

            const formData: FormData = new FormData();

            formData.append('logo', file);
             return this.http.put(`${this.api_logo}/${id}`, formData)

            //return this.http.put(`http://localhost:8084/logo/${id}`, formData)
        }

    getListTimeZones():Observable<string[]>{
        return this.http.get<string[]>(`${this.urlResource}/timezones`)
    }
}
