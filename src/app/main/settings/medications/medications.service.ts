import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {API_URL} from "app/_shared/config/api.url.config";
import {MedicationDto} from "./medication.model";
import {PageDto} from 'app/_shared/_models/page.model';

@Injectable({
    providedIn: 'root'
})
export class MedicationsService {

    private urlResource: string = API_URL.api_medications;

    constructor(private http: HttpClient) {
    }

    getAll(pageIndex: number=0, pageSize: number=10, keyword: string=''): Observable<PageDto<MedicationDto>> {
        return this.http.get<PageDto<MedicationDto>>(`${this.urlResource}`,
        {
            params: new HttpParams()
                .set('page',pageIndex.toString())
                .set('limit',pageSize.toString())
                .set('keyword',keyword)

        })
    }

    getOneAnnouncement(id: number): Observable<MedicationDto> {
        return this.http.get<MedicationDto>(`${this.urlResource}/${id}`)
    }

    delete(id: number): Observable<MedicationDto> {
        return this.http.delete<MedicationDto>(this.urlResource + `/${id}`);
    }

    add(medication: MedicationDto) {
        return this.http.post(this.urlResource, medication)
    }

    update(medication: MedicationDto) {
        return this.http.put(this.urlResource + `/${medication.id}`, medication)
    }


    deleteSelected(ids: number[]): Observable<MedicationDto> {
        // const token = this.authService.getToken();
        // if (token) {
        return this.http.post<MedicationDto>(`${this.urlResource}/deleteMultiRows`, ids
            //,{headers: new HttpHeaders({'Authorization': token})}
        );
        // } else {
        //     this.authService.logout();
        // }
    }

}
