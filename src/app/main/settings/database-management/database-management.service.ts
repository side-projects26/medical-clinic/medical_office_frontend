import { Injectable } from '@angular/core';
import {API_URL} from "../../../_shared/config/api.url.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {InfosDto} from "../infos/infos.model";

@Injectable({
  providedIn: 'root'
})
export class DatabaseManagementService {

    private urlResource:string=API_URL.api_database_management;
    constructor(private http:HttpClient) {
    }

    public getBackup(){
        return this.http.get(this.urlResource, {
            responseType: 'arraybuffer'
        });
    }


}
