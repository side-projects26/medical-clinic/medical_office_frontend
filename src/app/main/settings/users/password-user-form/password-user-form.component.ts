import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {UserRequest} from "../user.model";
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {of, Subject} from "rxjs";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {takeUntil} from "rxjs/internal/operators";

@Component({
  selector: 'app-password-user-form',
  templateUrl: './password-user-form.component.html',
  styleUrls: ['./password-user-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PasswordUserFormComponent implements OnInit, OnDestroy {
    action: string;
    user: UserRequest;
    passwordUserForm: FormGroup;
    dialogTitle: string;
    // optionSelected: string;
    // options=[
    //     {value: 'DOCTOR', viewValue: 'DOCTOR'},
    //     {value: 'SECRETARY', viewValue: 'SECRETARY'},
    //     {value: 'ADMIN', viewValue: 'ADMIN'}
    // ];
    roles = [];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<AppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<PasswordUserFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit User Password';
            this.user = _data.user;
            this.passwordUserForm = this.createEditPasswordUserForm();

        }
    }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    createEditPasswordUserForm(): FormGroup {
        let myformGroup: FormGroup = this._formBuilder.group({
            username: [this.user.username],
           // id: [this.user.id],
            password: [this.user.password, [Validators.required, Validators.minLength(8)]],
            confirmPassword: [this.user.confirmPassword, [Validators.required,
                confirmPasswordValidator
            ]
            ],
        });


        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        myformGroup.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                myformGroup.get('confirmPassword').updateValueAndValidity();
            });

        return myformGroup;
    }





    ngOnInit(): void {

    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};