import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordUserFormComponent } from './password-user-form.component';

describe('PasswordUserFormComponent', () => {
  let component: PasswordUserFormComponent;
  let fixture: ComponentFixture<PasswordUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
