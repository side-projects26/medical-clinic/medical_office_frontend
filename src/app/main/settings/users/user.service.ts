import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {API_URL} from "app/_shared/config/api.url.config";
import {Observable} from 'rxjs';
import {UserRequest, UserResponse} from './user.model';
import {PageDto} from 'app/_shared/_models/page.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private urlResource: string = API_URL.api_users;
    private api_change_password: string = API_URL.api_change_password;

    constructor(private http: HttpClient) {
    }

    getUsers(pageIndex: number, pageSize: number,
             keyword: string): Observable<PageDto<UserResponse>> {
        return this.http.get<PageDto<UserResponse>>(this.urlResource,
            {
                params: new HttpParams()
                    .set('page', pageIndex.toString())
                    .set('limit', pageSize.toString())
                    .set('keyword', keyword)

            })
    }

    // getRole(url:string):Observable<any>{
    //   return this.http.get(url);
    // }
    deleteUser(id: number): Observable<UserResponse> {
        return this.http.delete<UserResponse>(this.urlResource + `/${id}`);
    }

    addUser(user: UserRequest) {
        return this.http.post(this.urlResource, user)
    }

    updateUser(user: UserRequest) {
        return this.http.put(this.urlResource + `/${user.id}`, user)
    }

    //changePassword(username: string, password: string, confirmPassword: string) {
    changePassword(form) {
        const formData=new FormData();
        formData.set('username',form.username);
        formData.set('password',form.password);
        formData.set('confirmPassword',form.confirmPassword);
        return this.http.post(this.api_change_password, formData)
    }

}
