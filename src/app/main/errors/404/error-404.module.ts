import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';

import {FuseSharedModule} from '@fuse/shared.module';

import {Error404Component} from 'app/main/errors/404/error-404.component';
import {AuthGuard} from "../../../_shared/guards/auth.guard";

const routes = [
    {
        path: 'error-404',
        // path     : '**',
        component: Error404Component,
        canLoad: [AuthGuard]
    },
    /* {
       path: '**',
       redirectTo: '/error-404',
       pathMatch: 'full'
    }*/
// {
//     path: '**',
//     component: Error404Component,
//     canLoad:[AuthGuard]
// }
];

@NgModule({
    declarations: [
        Error404Component
    ],
    imports: [
        RouterModule.forChild(routes),

        MatIconModule,

        FuseSharedModule
    ]
})
export class Error404Module {
}
