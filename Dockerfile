# stage 1
#FROM node:latest as node
#WORKDIR /app
#COPY . .
#RUN npm install
#RUN npm run build --prod
#
## stage 2
#FROM nginx:alpine
#COPY nginx.conf /etc/nginx/conf.d/default.conf
#RUN rm -rf /usr/share/nginx/html/*
#COPY --from=node /app/dist/* /usr/share/nginx/html

# zakaria
FROM nginx:alpine
RUN echo "step 3"
# Arguments
#ARG ANGULAR_APP_API_BASE_URL
#ENV ANGULAR_APP_API_BASE_URL=${ANGULAR_APP_API_BASE_URL}
WORKDIR /app
COPY dist/* /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx","-g","daemon off;"]


# callicoder
#### Stage 1: Build the react application
#FROM node:15.14.0-alpine as build
#
## Configure the main working directory inside the docker image.
## This is the base directory used in any further RUN, COPY, and ENTRYPOINT
## commands.
#WORKDIR /app
#
## Copy the package.json as well as the package-lock.json and install
## the dependencies. This is a separate step so the dependencies
## will be cached unless changes to one of those two files
## are made.
#
#
#COPY . .
## Arguments
##ARG ANGULAR_APP_API_BASE_URL
##ENV ANGULAR_APP_API_BASE_URL=${ANGULAR_APP_API_BASE_URL}
#
#RUN npm install
#RUN npm run build --prod
#
##
## stage 2
#FROM nginx:1.17.0-alpine
#COPY nginx.conf /etc/nginx/conf.d/default.conf
#RUN rm -rf /usr/share/nginx/html/*
#COPY --from=build /app/dist/* /usr/share/nginx/html
##
#
## Expose port 80 to the Docker host, so we can access it
## from the outside.
#EXPOSE 80
#
#ENTRYPOINT ["nginx","-g","daemon off;"]
